<?php

namespace App\Console\Commands;

use App\Models\Offer;
use Illuminate\Console\Command;
use Carbon\Carbon;

class makeOffer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:Offer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'the vendor can make an Offer within 48 h';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $Offers = Offer::all();
        foreach ($Offers as $key => $value) {
            $date2 = Carbon::parse($value->created_at);
            $date1 = Carbon::now();
            $date3 = Carbon::parse($value->created_at);
            $date4 = Carbon::parse($value->created_at);
            $date5 = Carbon::parse($value->created_at);
            $date6 = Carbon::parse($value->created_at);
            $Friday = Carbon::now()->addHour(24)->format('l');
            $Saturday = Carbon::now()->addHour(48)->format('l');;
            if (
                $date5->addHour(48) < $date1
                and ($Friday != 'Friday' or $Saturday != 'Saturday')
                and $date2->format('l') != 'Friday'
                and $date2->format('l') != 'Saturday'
            ) {
                $y = Carbon::now()->subHour(24)->format('l');
                if ($y == 'Saturday') {
                    if ($date2->format('l') == 'Wednesday') {
                        if ($date4->addHour(48 + 48) < $date1) {
                            $value->hide = 1;
                            $value->save();
                        }
                    }
                    if ($date2->format('l') == 'Thursday') {
                        if ($date3->addHour(48 + 24) < $date1) {
                            $value->hide = 1;
                            $value->save();
                        }
                    }
                } else {
                    $value->hide = 1;
                    $value->save();
                }
            } else {
                $y = Carbon::now()->subHour(24)->format('l');
                if ($y == 'Saturday') {
                    if ($date2->format('l') == 'Wednesday') {
                        if ($date4->addHour(48 + 48) < $date1) {
                            $value->hide = 1;
                            $value->save();
                        }
                    }
                    if ($date2->format('l') == 'Thursday') {
                        if ($date3->addHour(48 + 24) < $date1) {
                            $value->hide = 1;
                            $value->save();
                        }
                    }
                } else if ($date6->addHour(48) < $date1) {
                    $value->hide = 1;
                    $value->save();
                }
            }
        }
        return Command::SUCCESS;
    }
}
