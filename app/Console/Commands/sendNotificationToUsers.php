<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class sendNotificationToUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notificationToUsers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command for sending notification ToUsers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $SERVER_API_KEY = 'AAAAETPFy5Q:APA91bE4lM1PuOO-dEsrpSpAPczRxJUrwI29ZcmkPhxTTDxBD0jLIL0KsDaIwSiGlm5Qdo9W8NfxtmMZ5mwszMqWM7g2kBrEWw427hkHcnNpXPb_4ghAFVmdNvOsKnNzj3FKCaNUtu82';
        $arr = array(
            "title" => " Hi Darling, Have a Good Day! 🤍💙",
            "body" =>  " Please send your photo during Qurantine 🤳🏻📸"
        );
        $users = User::all();
        foreach ($users as $user) {
            $device_tokens = $user->fcmToken;
            $notification = array('title' => " Hi Darling, Have a Good Day! 🤍💙", 'text' => " Please send your photo during Qurantine 🤳🏻📸", 'sound' => 'default', 'badge' => '1');
            $data = [
                "registration_ids" => array($device_tokens), // for multiple device ids
                'notification' => $notification,
                "data" => $arr
            ];
            $dataString = json_encode($data);
            $headers = [
                'Authorization: key=' . $SERVER_API_KEY,
                'Content-Type: application/json',
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            $response = curl_exec($ch);
        }
    }
}
