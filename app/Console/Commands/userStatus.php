<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class userStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check user days in qurantine on daily (through 7 days)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user) {
            $date2 = Carbon::parse($user->created_at)->addDays(7);
            $date1 = Carbon::now();
            if ($date1 > $date2) {
                $user->status = 0;
            }
            $user->days -= 1;
            $user->save();
        }
    }
}
