<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Medical_center;
use App\Models\Medical_vaccine;
use App\Models\Vaccine;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class Medical_centerApiController extends Controller
{
    //
    public function index()
    {
        $items = User::role('Medical_center')->get();
        foreach ($items as $item) {
            $Medical_center = Medical_center::where('user_id', $item->id)->first();
            $vaccineCenter = Medical_vaccine::where('center_id', $item->id)->get();
            $vaccineIds = $vaccineCenter->pluck('vaccine_id');
            $vaccines = Vaccine::whereIn('id', $vaccineIds)->get();;
            $item->Medical_center = $Medical_center;
            $item->vaccines = $vaccines;
        }
        return $items;
    }

    public function store(Request $request)
    {
        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->password = Hash::make($request->password);
        $items->save();
        $items->assignRole('Medical_center');
        $Medical_center = new Medical_center();
        $Medical_center->user_id = $items->id;
        $Medical_center->address = $items->address;
        $Medical_center->time_open =  $request->time_open;
        $Medical_center->phone =  $request->phone;

        $Medical_center->save();
        return $items;
    }

    public function show($id)
    {
        $items = Medical_center::find($id);
        return $items;
    }

    public function update(Request $request, $id)
    {

        $items = User::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->phone) {
            $items->phone = $request->phone;
        }
        if ($request->time_open) {
            $items->time_open = $request->time_open;
        }
        $items->save();
        return $items;
    }


    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        $hospital = Medical_center::where('user_id', $id)->first();
        $hospital->delete();
        return 'Deleted';
    }

    public function showVaccinee($id)
    {
        // هون عم نجيب كل الفيلدز بتيبل الكرس يلي بتحمل ايدي معين رجعنالو ياه
        $medicalCenterVasccine =  Medical_vaccine::where('center_id', $id)->get();
        // هاد المتغير بيحوي على كولكشن جيسون فيها كلشي بقلب تيبل الربط يلي بيخص السيرنتر ها
        // بدي من هاد المتغير مصفوفة بتحتوي على ايديات الفاكسينس لهيك استخدمت فنكشن البلك عال فاكسين ايدي
        $medicalCenterVasccineIDS = $medicalCenterVasccine->pluck('vaccine_id');
        // هلا هون بتعليمة وير ان عطيتو مصفوفة ايديات الفاكسينيز وقلتلو رجعلي ياهن وهيك بحصل على الفاكسينز يلي بيخصو هاد السينتر
        $items = Vaccine::whereIn('id', $medicalCenterVasccineIDS)->get();
        // هون مشان مانعمل فيو خاصة بالفاكسينز تبع السينتر استخدمنا نفس الفيو تبع الفاكسينز يلي نحن اوريدي عاملينا سابقا
        return $items;
    }
}
