<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Models\Medical_center;
use App\Models\Medical_vaccine;
use App\Models\User;
use App\Models\Vaccine;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class Medical_centerController extends Controller
{
    //
    public function index()
    {
        $items = User::role('Medical_center')->get();
        return view('admin.medical_center.index', compact('items'));
    }
    public function profile()
    {
        $items = User::find(auth()->user()->id);
        $items->medical = Medical_center::where('user_id', $items->id)->first();
        $m = Medical_center::where('user_id', $items->id)->first();
        return view('admin.medical_center.detail', compact('items','m'));
    }
    public function detail($id)
    {
        $items = User::role('Medical_center')->find($id);
        $m = Medical_center::where('user_id',  $items->id)->first();
        return view('admin.medical_center.detail', compact('items','m'));
    }
    public function create()
    {
        
        return view('admin.medical_center.create');
    }

    public function store(Request $request)
    {
        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->address = $request->address;
        $items->password = Hash::make($request->password);
        $items->save();
        $items->assignRole('Medical_center');
        $Medical_center = new Medical_center();
        $Medical_center->user_id = $items->id;
        $Medical_center->time_open =  $request->time_open;
        $Medical_center->phone =  $request->phone;
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/Medical_center/images'));
        }
        $Medical_center->save();
        return redirect()->route('medical_center.index');
    }

    public function edit($id)
    {
        $items = User::find($id);
        return view('admin.medical_center.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = User::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->phone) {
            $items->phone = $request->phone;
        }
       
        if ($request->time_open) {
            $m = Medical_center::where('user_id', $items->id)->first();
            if ($m){
            $m->time_open = $request->time_open;
            $m->save();
        }}

        $items->save();
        //$m->save();
       
        return redirect()->route('medical_center.index');
    }

    public function showAppointment( $id)
    {
        $items = Appointment::where('center_id', $id)->get();
        return view('admin.medical_center.Appointment', compact('items'));
    }
    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        return redirect()->route('medical_center.index');
    }

    public function assignVaccineToCenter($id)
    {
        $items = User::find($id);
        $vaccines = Vaccine::all();
        return view('admin.assign_vaccine.create', compact('items', 'vaccines'));
    }
    public function editVacc($id)
    {
        $items = Vaccine::find($id);
        return view('admin.vaccine.editCenter', compact('items'));
    }
    public function assignVaccineStore(Request $request)
    {
        $medicalCenterVasccine = new Medical_vaccine();
        $medicalCenterVasccine->vaccine_id = $request->vaccine_id;
        $medicalCenterVasccine->center_id = $request->center_id;
        $medicalCenterVasccine->qty = $request->qty;
        $medicalCenterVasccine->save();
        return redirect()->route('medical_center.index');
    }

    public function assignVaccine(Request $request)
    {
        $medicalCenterVasccine = new Medical_vaccine();
        $medicalCenterVasccine->vaccine_id = $request->vaccine_id;
        $medicalCenterVasccine->center_id = $request->center_id;
        $medicalCenterVasccine->qty = $request->qty;
        $medicalCenterVasccine->save();
        return redirect()->route('admin.assign_vaccine.create');
    }
    public function showVaccine($id)
    {
        // هون عم نجيب كل الفيلدز بتيبل الكرس يلي بتحمل ايدي معين رجعنالو ياه
        $medicalCenterVasccine =  Medical_vaccine::where('center_id', $id)->get();
        // هاد المتغير بيحوي على كولكشن جيسون فيها كلشي بقلب تيبل الربط يلي بيخص السيرنتر ها
        // بدي من هاد المتغير مصفوفة بتحتوي على ايديات الفاكسينس لهيك استخدمت فنكشن البلك عال فاكسين ايدي
        $medicalCenterVasccineIDS = $medicalCenterVasccine->pluck('vaccine_id');
        // هلا هون بتعليمة وير ان عطيتو مصفوفة ايديات الفاكسينيز وقلتلو رجعلي ياهن وهيك بحصل على الفاكسينز يلي بيخصو هاد السينتر
        $items = Vaccine::whereIn('id', $medicalCenterVasccineIDS)->get();
        foreach($items as $item) {
            $medicalCenter = Medical_vaccine::where('center_id', $id)->where('vaccine_id', $item->id)->first();
            $item->medicalCenter = $medicalCenter;
            }
        // هون مشان مانعمل فيو خاصة بالفاكسينز تبع السينتر استخدمنا نفس الفيو تبع الفاكسينز يلي نحن اوريدي عاملينا سابقا
        return view('admin.vaccine.index', compact('items'));
    }
    public function showmyVaccine()
    {
        $medicalCenterVasccine =  Medical_vaccine::where('center_id', auth()->user()->id)->get();
        $medicalCenterVasccineIDS = $medicalCenterVasccine->pluck('vaccine_id');
        $items = Vaccine::whereIn('id', $medicalCenterVasccineIDS)->get();
        foreach($items as $item) {
            $medicalCenter = Medical_vaccine::where('center_id', auth()->user()->id)->where('vaccine_id', $item->id)->first();
            $item->medicalCenter = $medicalCenter;
            }
        return view('admin.vaccine.index', compact('items'));
    }
}
