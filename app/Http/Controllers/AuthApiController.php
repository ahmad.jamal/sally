<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthApiController extends Controller
{
    public function store(Request $request)
    {
        // dd($request);
        // return $request;
        $request->validate([
            'email' => 'email|unique:users',
        ]);
        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->password = Hash::make($request->password);
        $items->address =  $request->address;
        $items->save();

        $items->assignRole('User');
        return redirect()->route('user.index');
    }
}
