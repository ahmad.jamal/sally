<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Spatie\Permission\Models\Role;

class CategoryController extends Controller
{

    public function index()
    {
        $items = Category::all();
        return view('admin.category.index', compact('items'));
    }

    public function create()
    {
        return view('admin.category.create');
    }

    public function store(Request $request)
    {
        $items = new Category();
        $items->name = $request->name;   
        $items->product_id = $request->product_id;     
        $items->save();
        
        return redirect()->route('category.index');
    }

    public function edit($id)
    {
        $items = Category::find($id);
        return view('admin.category.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = Category::find($id);
        if ($request->name) {
            $items->name = $request->name;
           
        }
        if ($request->product_id) {
            $items->product_id = $request->product_id;
        }

        
        $items->save();
        return redirect()->route('category.index');
    }


    public function destroy($id)
    {
        $items = Category::find($id);
        $items->delete();
        return redirect()->route('category.index');
    }
}
