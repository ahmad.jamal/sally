<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;
use App\Models\Medical_center;
use App\Models\Medical_vaccine;
use App\Models\Vaccine;

class AppointmentApiController extends Controller
{
    public function index()
    {
        $items = Appointment::all();
        return $items;
    }

    public function store(Request $request)
    {
        $center = Medical_center::where('user_id', $request->center_id)->first();
        $items = new Appointment();
        $items->user_id = $request->user_id;
        $items->date = $request->date;
        $items->center_id = $center->id;
        $items->vaccine_id = $request->vaccine_id;
        $Vaccine_Center = Medical_vaccine::where('vaccine_id', $request->vaccine_id)
            ->where('center_id', $request->center_id)
            ->first();
        if (!empty($Vaccine_Center)) {
            $Vaccine_Center->qty -= 1;
            $Vaccine_Center->save();
            if ($Vaccine_Center->qty == 0) {
                $Vaccine_Center->delete();
            }
        } else {
            return 0;
        }
        $items->save();
        return $items;
    }

    public function show($id)
    {
        $items = Appointment::find($id);
        return $items;
    }

    public function update(Request $request, $id)
    {
        $items = Appointment::find($id);
        if ($request->user_id) {
            $items->user_id = $request->user_id;
        }
        if ($request->date) {
            $items->date = $request->date;
        }
        if ($request->center_id) {
            $items->center_id = $request->center_id;
        }
        $items->save();
        return $items;
    }
    public function destroy($id)
    {
        $items = Appointment::find($id);
        $items->delete();
        return 'Deleted';
    }
}
