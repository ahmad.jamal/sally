<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $items = User::role('User')->get();
        return view('admin.user.index', compact('items'));
    }
    public function profile($id)
    {
        $user = User::find($id);
        return view('admin.user.profile', compact('user'));
    }
   
    public function create()
    {
        return view('admin.user.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'email|unique:users',
        ]);
        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->password = Hash::make($request->password);
        $items->address =  $request->address;
        $items->phone =  $request->phone;
        $items->save();

        $items->assignRole('User');
        return redirect()->route('user.index');
    }
    public function edit($id)
    {
        $items = User::find($id);
        return view('admin.user.edit', compact('items', 'roles'));
    }
    public function update(Request $request, $id)
    {

        $items = User::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
       
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->phone) {
            $items->phone = $request->phone;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/user/images'));
        }
        $items->save();
        return redirect()->route('user.index');
    }
    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        return redirect()->route('user.index');
    }
    public function shangeStatus($id)
    {
        $user = User::find($id);
    
        if ($user->pand == 1) {
            $user->pand = 0;
        } else {
            $user->pand = 1;
        }
        $user->save();
        return redirect()->route('user.index');
    }
   
}
