<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vaccine;

class VaccineController extends Controller
{
    public function index()
    {
        $items = Vaccine::all();
        return view('admin.vaccine.index', compact('items'));
    }

    public function create()
    {
        $vaccines = Vaccine::all();
        return view('admin.vaccine.create',compact('vaccines'));
    }

    public function store(Request $request)
    {
        $items = new Vaccine();
        $items->name = $request->name;      
        $items->description = $request->description;                                                  
        $items->save();
        
        return redirect()->route('vaccine.index');
    }

    public function edit($id)
    {
        $items = Vaccine::find($id);
        return view('admin.vaccine.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = Vaccine::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->description) {
            $items->description = $request->description;
        }
        $items->save();
        return redirect()->route('vaccine.index');
    }


    public function destroy($id)
    {
        $items = Vaccine::find($id);
        $items->delete();
        return redirect()->route('vaccine.index');
    }
    
}
