<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Voice;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use JWTAuth;

class UserApiController extends Controller
{
    public function imageStore(Request $request)
    {
        $item = new Image();
        $item->user_id = $request->user_id;
        if ($request->hasFile('image')) {
            $item->link = str_replace('public', 'storage', $request->image->store('public/user/images'));
        }
        $item->save();
        return $item;
    }
    public function voiceStore(Request $request)
    {
        $item = new Voice();
        $item->user_id = $request->user_id;
        if ($request->hasFile('voice')) {
            $item->link = str_replace('public', 'storage', $request->voice->store('public/user/images'));
        }
        $item->save();
        return $item;
    }
    public function index()
    {
        $items = User::all();
        return $items;
    }

    public function store(Request $request)
    {
        // dd($request);
        // return $request;
        $request->validate([
            'email' => 'email|unique:users',
        ]);
        $items = new User();
        $items->name = $request->name;
        $items->national_number =  $request->national_number;
        $items->password = Hash::make($request->password);
        $items->address =  $request->address;
        $items->phone =  $request->phone;
        $items->save();

        $items->assignRole('User');


        $validate['email'] = $request->national_number;
        $credentials['password'] = $request->password;

        config()->set('auth.defaults.guard', 'api');
        \Config::set('jwt.user', 'App\Models\User');
        \Config::set('auth.providers.users.model', \App\Models\User::class);

        try {

            if (!$token = Auth::guard('api')->attempt($credentials)) {
                return response()->json(['error' => 'the email or password not correct'], 200);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        if (request()->is('api/*')) {
            return $this->respondWithToken($token);
        } else {
            return redirect()->route('dash')->with(['success' => 'تم الحفظ بنجاح']);
        }






        $token = auth()->login($items);
        //return $token;
        return $this->respondWithToken($token);
        return $items;
    }
    public function logout()
    {
        Auth::guard('api')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
    public function authenticate(Request $request)
    {
        //return "ahmad";
        // Retrive Input
        $credentials = request(['national_number', 'password']);

        //return $credentials;
        config()->set('auth.defaults.guard', 'api');
        \Config::set('jwt.user', 'App\Models\User');
        \Config::set('auth.providers.users.model', \App\Models\User::class);
        $myTTL = 60000; //minutes
        JWTAuth::factory()->setTTL($myTTL);

        try {

            if (!$token = Auth::guard('api')->attempt($credentials)) {
                return response()->json(['message' => 'Sorry, Incorrect Email Or Password'], 200);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'could_not_create_token'], 200);
        }
        if (request()->is('api/*')) {
            return $this->respondWithToken($token);
        } else {
            return redirect()->route('dash')->with(['success' => 'تم الحفظ بنجاح']);
        }
    }
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'user' => Auth::guard('api')->user(),
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 15768000
        ]);
    }
    public function show($id)
    {
        $items = User::find($id);
        return $items;
    }

    public function update(Request $request, $id)
    {


        $items = User::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->role_name) {
            $items->assignRole($request->role_name);
        }
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/user/images'));
        }
        $items->save();
        return $items;
    }
    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        return 'Deleted';
    }
}
