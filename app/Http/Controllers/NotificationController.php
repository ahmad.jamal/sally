<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\User;
use App\Models\Notification;

class NotificationController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('admin.notification.add', compact('users'));
    }
    public function indexApi()
    {
        return Notification::latest('created_at')->get()->take(25);
    }

    public function send(Request $request)
    {
        $SERVER_API_KEY = 'AAAAETPFy5Q:APA91bE4lM1PuOO-dEsrpSpAPczRxJUrwI29ZcmkPhxTTDxBD0jLIL0KsDaIwSiGlm5Qdo9W8NfxtmMZ5mwszMqWM7g2kBrEWw427hkHcnNpXPb_4ghAFVmdNvOsKnNzj3FKCaNUtu82';
        $arr = array(
            "title" => $request->title,
            "body" =>  $request->body
        );
        if ($request['users'][0] == 0) {
            $users = User::all();
        } else {
            $users = User::whereIn('id', $request->users)->get();
        }
        foreach ($users as $user) {
            $device_tokens = $user->fcmToken;
            $notification = array('title' => $request->title, 'text' => $request->body, 'sound' => 'default', 'badge' => '1');
            $data = [
                "registration_ids" => array($device_tokens), // for multiple device ids
                'notification' => $notification,
                "data" => $arr
            ];
            $dataString = json_encode($data);
            $headers = [
                'Authorization: key=' . $SERVER_API_KEY,
                'Content-Type: application/json',
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            $response = curl_exec($ch);

            return $response;

            $notification = new Notification();
            $notification->user_sent_to_id = $user->id;
            $notification->admin_sender_id = auth()->user()->id;
            $notification->title = $request->title;
            $notification->save();

            // notification
        }
        return redirect()->back();
    }
}
