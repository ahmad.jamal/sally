<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hospital;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class HospitalApiController extends Controller
{
    public function index()
    {
        $items = User::role('Hospital')->get();
        foreach ($items as $item) {
            $hospital = Hospital::where('user_id', $item->id)->get();
            $item->hospital = $hospital;
        }
        return $items;
    }

    public function store(Request $request)
    {
        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->password = Hash::make($request->password);
        $items->save();
        $items->assignRole('Hospital');
        $hospital = new Hospital();
        $hospital->user_id = $items->id;
        $hospital->night_price = $request->night_price;
        $hospital->save();
        $items->hospital = $hospital;
        return $items;
    }
    public function show($id)
    {
        $items = User::role('Hospital')->find($id);
        $hospital = Hospital::where('user_id', $items->id)->get();
        $items->hospital = $hospital;
        return $items;
    }

    public function update(Request $request, $id)
    {

        $items = User::find($id);
        $hospital = Hospital::where('user_id', $items->id)->first();
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->night_price) {
            $hospital->night_price = $request->night_price;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/hospital/images'));
        }
        $items->save();
        $hospital->save();
        $items->hospital = $hospital;
        return $items;
    }


    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        $hospital = Hospital::where('user_id', $id)->first();
        $hospital->delete();
        return 'Deleted';
    }
}
