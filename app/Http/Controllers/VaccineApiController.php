<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vaccine;

class VaccineApiController extends Controller
{
    public function index()
    {
        $items = Vaccine::all();
        return $items;
    }


    public function store(Request $request)
    {
        $items = new Vaccine();
        $items->name = $request->name;      
        $items->description = $request->description;                                                  
        $items->save();
        
        return $items;
    }

    public function show($id)
    {
        $items = Vaccine::find($id);
        return $items;
    }


    public function update(Request $request, $id)
    {

        $items = Vaccine::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->description) {
            $items->description = $request->description;
        }
        $items->save();
        return $items;
    }


    public function destroy($id)
    {
        $items = Vaccine::find($id);
        $items->delete();
        return $items;
    }
}
