<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pharmacy;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class PharmacyController extends Controller
{
    public function index()
    {
        $items = User::role('Pharmacy')->get();
        return view('admin.pharmacy.index', compact('items'));
    }

    public function detail($id)
    {
        $items = User::role('Pharmacy')->find($id);
        return view('admin.pharmacy.profile', compact('items'));
    }

    public function create()
    {

        return view('admin.pharmacy.create');
    }

    public function store(Request $request)
    {
        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->password = Hash::make($request->password);
        $items->save();
        $items->assignRole('Pharmacy');
        $pharmacy = new Pharmacy();
        $pharmacy->user_id = $items->id;
        $pharmacy->location =  $request->location;
        $pharmacy->phone =  $request->phone;
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/pharmacy/images'));
        }
        $pharmacy->save();
        return redirect()->route('pharmacy.index');
    }

    public function edit($id)
    {
        $items = User::find($id);
        return view('admin.pharmacy.edit', compact('items'));
    }

    public function profile()
    {
        $items = User::find(auth()->user()->id);
        $items->pharmacy = Pharmacy::where('user_id', $items->id)->first();
        return view('admin.pharmacy.profile', compact('items'));
    }
    public function update(Request $request, $id)
    {

        $items = User::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->location) {
            $items->address = $request->location;
        }
        if ($request->phone) {
            $items->phone = $request->phone;
        }

        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/pharmacy/images'));
        }
        $items->save();
        return redirect()->route('pharmacy.index');
    }


    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        return redirect()->route('pharmacy.index');
    }
}
