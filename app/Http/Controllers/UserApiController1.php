<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Voice;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use Illuminate\Support\Facades\Http;

class UserApiController1 extends Controller
{
    public function imageStore(Request $request)
    {
        $item = new Image();
        $item->user_id = $request->user_id;
        if ($request->hasFile('image')) {
            $item->link = str_replace('public', 'storage', $request->image->store('public/user/images'));
        }
        $item->save();
        // This Is The Code For Sending Requset To AI face API
        return Http::attach('picture_1', file_get_contents($request->file('image')), 'image.png')
            ->attach('picture_2', file_get_contents($request->file('image')), 'image.png')
            ->post('https://senior-ai-system.herokuapp.com/face');
    }
    public function voiceStore(Request $request)
    {
        $item = new Voice();
        $item->user_id = $request->user_id;
        if ($request->hasFile('voice')) {
            $item->link = str_replace('public', 'storage', $request->voice->store('public/user/voice'));
        }
        $item->save();
        // This Is The Code For Sending Requset To AI voice API
        return Http::attach('audio', file_get_contents($request->file('voice')), 'image.png')
            ->post('https://senior-ai-system.herokuapp.com/voice');
       
    }
    public function index()
    {
        $items = User::all();
        return $items;
    }

    public function store(Request $request)
    {
        $items = new User();
        $items->name = $request->name;
        $items->national_number =  $request->national_number;
        $items->password = Hash::make($request->password);
        $items->address =  $request->address;
        $items->long =  $request->long;
        $items->lat =  $request->lat;
        $items->phone =  $request->phone;
        $items->save();

        $items->assignRole('User');

        $credentials['national_number'] = $items->national_number;
        $credentials['password'] = $items->password;

        config()->set('auth.defaults.guard', 'api');
        \Config::set('jwt.user', 'App\Models\User');
        \Config::set('auth.providers.users.model', \App\Models\User::class);
        // return $credentials;
        $token = auth()->login($items);
        //return $token;

        $SERVER_API_KEY = 'AAAAETPFy5Q:APA91bE4lM1PuOO-dEsrpSpAPczRxJUrwI29ZcmkPhxTTDxBD0jLIL0KsDaIwSiGlm5Qdo9W8NfxtmMZ5mwszMqWM7g2kBrEWw427hkHcnNpXPb_4ghAFVmdNvOsKnNzj3FKCaNUtu82';
        $arr = array(
            "title" => "Good Morning Darling 🤍💙",
            "body" =>  "Please don't forget to send your photo and record your voice🤳🏻📸"
        );
        $users = $items;

        $device_tokens = $users->fcmToken;
        $notification = array('title' => "Good Morning Darling 🤍💙", 'text' => "Please don't forget to send your photo and record your voice 🤳🏻📸", 'sound' => 'default', 'badge' => '1');
        $data = [
            "registration_ids" => array($device_tokens), // for multiple device ids
            'notification' => $notification,
            "data" => $arr
        ];
        $dataString = json_encode($data);
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);

        return $this->respondWithToken($token);
        try {

            if (!$token = Auth::guard('api')->attempt($credentials)) {
                return response()->json(['error' => 'the national number or password not correct'], 404);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        if (request()->is('api/*')) {
            return $this->respondWithToken($token);
        } else {
            return redirect()->route('dash')->with(['success' => 'تم الحفظ بنجاح']);
        }

        return $items;
    }
    public function login(Request $request)
    {

        $credentials = request(['national_number', 'password']);

        // return $credentials;
        config()->set('auth.defaults.guard', 'api');
        \Config::set('jwt.user', 'App\Models\User');
        \Config::set('auth.providers.users.model', \App\Models\User::class);
        $myTTL = 60000; //minutes
        JWTAuth::factory()->setTTL($myTTL);

        try {

            if (!$token = Auth::guard('api')->attempt($credentials)) {

                return response()->json(['message' => 'Sorry, Incorrect Email Or Password'], 404);

                return response()->json(['message' => 'Sorry, National Number Or Password are not correct'], 404);
            }
        } catch (JWTException $e) {
            return response()->json(['message' => 'could_not_create_token'], 404);
        }
        if (request()->is('api/*')) {
            return $this->respondWithToken($token);
        } else {
            return redirect()->route('dash')->with(['success' => 'تم الحفظ بنجاح']);
        }
    }

    public function logout()
    {
        Auth::guard('api')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'user' => Auth::guard('api')->user(),
            'token_type' => 'bearer',
            'expires_in' => Auth::guard('api')->factory()->getTTL() * 15768000
        ]);
    }
    public function show($id)
    {
        $items = User::find($id);
        return $items;
    }

    public function update(Request $request)
    {

        $items = User::find($request->user_id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->role_name) {
            $items->assignRole($request->role_name);
        }
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->long) {
            $items->long = $request->long;
        }
        if ($request->lat) {
            $items->lat = $request->lat;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/user/images'));
        }
        $items->save();
        return $items;
    }
    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        return 'Deleted';
    }
}
