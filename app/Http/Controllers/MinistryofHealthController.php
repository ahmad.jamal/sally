<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ministry_of_health;
use Spatie\Permission\Models\Role;

class MinistryofHealthController extends Controller
{
    public function index()
    {
        $items = Ministry_of_health::all();
        return view('admin.ministry.index', compact('items'));
    }
    public function profile($id)
    {
        $user = Ministry_of_health::find($id);
        return view('admin.ministry.profile', compact('user'));
    }

    public function create()
    {
        
        return view('admin.ministry.create');
    }

    public function store(Request $request)
    {
        $items = new Ministry_of_health();
        $items->name = $request->name;
        $items->age = $request->age;
        $items->gender =  $request->gender;
        $items->pcr_test =  $request->pcr_test;
        $items->national_number = $request->national_number;
        $items->address = $request->address;
        $items->save();
        
       
        
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/users/images'));
        }
        $items->save();
        return redirect()->route('ministry.index');
    }

    public function edit($id)
    {
        $items = Ministry_of_health::find($id);
        return view('admin.ministry.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = Ministry_of_health::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->age) {
            $items->age = $request->age;
        }
        if ($request->gender) {
            $items->gender = $request->gender;
        }
        if ($request->gender) {
            $items->gender = $request->gender;
        }
        if ($request->pcr_test) {
            $items->pcr_test = $request->pcr_test;
        }
        if ($request->national_number) {
            $items->national_number = $request->national_number;
        }
        if ($request->address) {
            $items->address = $request->address;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/users/images'));
        }
        $items->save();
        return redirect()->route('ministry.index');
    }


    public function destroy($id)
    {
        $items = Ministry_of_health::find($id);
        $items->delete();
        return redirect()->route('ministry.index');
    }
}
