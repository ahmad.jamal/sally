<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Brand;
use App\Models\Category;
use App\Models\User;
use App\Models\Pharmacy;
use Spatie\Permission\Models\Role;

class ProductController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $items = Product::where('pharmacy_id', $user->id)->get();
        return view('admin.product.index', compact('items'));
    }
    public function detail($id)
    {
        $p = Product::find($id);
        $ph = User::find(auth()->user()->id);
        $ph->pharma = Pharmacy::where('user_id', $ph->id)->first();
        return view('admin.product.detail', compact('p', 'ph'));
    }

    public function create()
    {
        $brands = Brand::all();
        $categories = Category::all();
        return view('admin.product.create', compact('brands', 'categories'));
    }

    public function store(Request $request)
    {
        $items = new Product();
        $items->name = $request->name;
        $items->description = $request->description;
        $items->price = $request->price;
        // $items->brand_id = $request->brand_id;
        // $items->category_id = $request->category_id;
        $items->pharmacy_id = auth()->user()->id;
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/product/images'));
        }
        $items->save();

        return redirect()->route('product.index');
    }

    public function edit($id)
    {
        $items = Product::find($id);
        return view('admin.product.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = Product::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->description) {
            $items->description = $request->description;
        }
        if ($request->price) {
            $items->price = $request->price;
        }
        if ($request->brand_id) {
            $items->brand_id = $request->brand_id;
        }
        if ($request->category_id) {
            $items->category_id = $request->category_id;
        }

        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/product/images'));
        }
        $items->save();
        return redirect()->route('product.index');
    }


    public function destroy($id)
    {
        $items = Product::find($id);
        $items->delete();
        return redirect()->route('product.index');
    }

    public function getPharmacyProducts($id)
    {
        $pp = Product::where('pharmacy_id', $id)->get();
        return $pp;
    }
}
