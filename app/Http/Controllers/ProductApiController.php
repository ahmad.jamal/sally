<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductApiController extends Controller
{
    public function index()
    {
        $items = Product::all();
        return $items;
    }


    public function store(Request $request)
    {
        $items = new Product();
        $items->name = $request->name;      
        $items->description = $request->description;  
        $items->price = $request->price;     
        $items->brand_id = $request->brand_id; 
        $items->category_id = $request->category_id; 
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/product/images'));
        }                                                    
        $items->save();
        
        return $items;
    }

    public function show($id)
    {
        $items = Product::find($id);
        return $items;
    }


    public function update(Request $request, $id)
    {

        $items = Product::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->description) {
            $items->description = $request->description;
        }
        if ($request->price) {
            $items->price = $request->price;
        }
        if ($request->brand_id) {
            $items->brand_id = $request->brand_id;
        }
        if ($request->category_id) {
            $items->category_id = $request->category_id;
        }
        
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/product/images'));
        }
        $items->save();
        return $items;
    }


    public function destroy($id)
    {
        $items = Product::find($id);
        $items->delete();
        return 'Deleted';
    }

}
