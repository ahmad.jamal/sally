<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        // dd($credentials);
        if (Auth::attempt($credentials)) {
            if (Auth::user()->pand == 1) {
                Auth::logout();
                return redirect()->route('login');
            }
            // return Auth::user()->hasRole('Admin');
            if (Auth::user()->hasRole('Admin')) {
                return redirect('/user');
            } else if (Auth::user()->hasRole('Pharmacy')) {
                return redirect()->route('pharmacy.profile');
            } else if (Auth::user()->hasRole('Hospital')) {
                return redirect()->route('hospital.profile');
            } else if (Auth::user()->hasRole('Medical_center')) {
                return redirect()->route('medical_center.profile');
            }
            return redirect('/user');
        }
        return redirect()->route('login')->with(['success' => 'كلمة المرور او البريد الالكتروني غير صحيح']);
    }
}
