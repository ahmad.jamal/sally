<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pharmacy;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class PharmacyApiController extends Controller
{
    public function index()
    {
        $items = User::role('Pharmacy')->get();
        foreach ($items as $item) {
            $pharmacy = Pharmacy::where('user_id', $item->id)->get();
            $item->pharmacy = $pharmacy;
        }
        return $items;
    }

    public function store(Request $request)
    {
        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->password = Hash::make($request->password);
        $items->save();
        $items->assignRole('Pharmacy');
        $pharmacy = new Pharmacy();
        $pharmacy->user_id = $items->id;
        $pharmacy->location =  $request->location;
        $pharmacy->phone =  $request->phone;
        $pharmacy->save();
        $items->pharmacy = $pharmacy;
        return $items;
    }

    public function show($id)
    {
        $items = User::role('Pharmacy')->find($id);
        $pharmacy = Pharmacy::where('user_id', $items->id)->get();
        $items->pharmacy = $pharmacy;
        return $items;
    }

    public function update(Request $request, $id)
    {

        $items = User::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        $pharmacy = Pharmacy::where('user_id', $items->id)->first();
        if ($request->location) {
            $pharmacy->location = $request->location;
        }
        if ($request->phone) {
            $pharmacy->phone = $request->phone;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/pharmacy/images'));
        }
        $items->save();
        $pharmacy->save();
        $items->pharmacy = $pharmacy;
        return $items;
    }


    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        $pharmacy = Pharmacy::where('user_id', $id)->first();
        $pharmacy->delete();
        return 'Deleted';
    }
}
