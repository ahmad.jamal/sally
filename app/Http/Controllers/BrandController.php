<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use Spatie\Permission\Models\Role;

class BrandController extends Controller
{
    public function index()
    {
        $items =Brand::all();
        return view('admin.brand.index', compact('items'));
    }

    public function create()
    {
        return view('admin.brand.create');
    }

    public function store(Request $request)
    {
        $items = new Brand();
        $items->name = $request->name;             
        $items->product_id = $request->product_id;                                              
        $items->save();
        
        return redirect()->route('brand.index');
    }

    public function edit($id)
    {
        $items = Brand::find($id);
        return view('admin.brand.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = Brand::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->product_id) {
            $items->name = $request->product_id;
        }

        $items->save();
        return redirect()->route('brand.index');
    }


    public function destroy($id)
    {
        $items = Brand::find($id);
        $items->delete();
        return redirect()->route('brand.index');
    }
}


