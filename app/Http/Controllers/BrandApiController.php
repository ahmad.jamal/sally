<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;

class BrandApiController extends Controller
{
    public function index()
    {
        $items =Brand::all();
        return $items;
    }


    public function store(Request $request)
    {
        $items = new Brand();
        $items->name = $request->name;
        $items->product_id = $request->product_id;                                                           
        $items->save();
        
        return $items;
    }

    public function show($id)
    {
        $items = Brand::find($id);
        return $items;
    }



    public function update(Request $request, $id)
    {

        $items = Brand::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }

        $items->save();
        return $items;
    }


    public function destroy($id)
    {
        $items = Brand::find($id);
        $items->delete();
        return 'Deleted';
    }
}
