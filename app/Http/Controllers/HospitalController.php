<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hospital;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class HospitalController extends Controller
{
    public function index()
    {
        $items = User::role('Hospital')->get();
        return view('admin.hospital.index', compact('items'));
    }

    public function detail($id)
    {
        $items = User::role('Hospital')->find($id);
        return view('admin.hospital.profile', compact('items'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('admin.hospital.create');
    }
    public function profile()
    {
        $items = User::find(auth()->user()->id);
        $items->hospital = Hospital::where('user_id', $items->id)->first();
        return view('admin.hospital.profile', compact('items'));
    }
    public function store(Request $request)
    {

        $items = new User();
        $items->name = $request->name;
        $items->email =  $request->email;
        $items->address =  $request->address;
        $items->phone =  $request->phone;
        $items->password = Hash::make($request->password);
        $items->save();
        $items->assignRole('Hospital');
        $hospital = new Hospital();
        $hospital->user_id = $items->id;
        $hospital->night_price =  $request->night_price;
        $hospital->phone =  $request->phone;
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/hospital/images'));
        }
        $hospital->save();
        return redirect()->route('hospital.index');
    }

    public function edit($id)
    {
        $items = User::find($id);
        return view('admin.hospital.edit', compact('items'));
    }


    public function update(Request $request, $id)
    {

        $items = User::find($id);

        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->night_price) {
            $items->night_price = $request->night_price;
        }
        if ($request->location) {
            $items->address = $request->location;
        }
        if ($request->phone) {
            $items->phone = $request->phone;
        }
        if ($request->hasFile('image')) {
            $items->image = str_replace('public', 'storage', $request->image->store('public/hospital/images'));
        }
        $items->save();
        return redirect()->route('hospital.index');
    }


    public function destroy($id)
    {
        $items = User::find($id);
        $items->delete();
        return redirect()->route('hospital.index');
    }
}
