<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryApiController extends Controller
{
    public function index()
    {
        $items = Category::all();
        return $items;
    }


    public function store(Request $request)
    {
        $items = new Category();
        $items->name = $request->name;
        $items->product_id = $request->product_id;
        $items->save();

        return $items;
    }

    public function show($id)
    {
        $items = Category::find($id);
        return $items;
    }

    public function update(Request $request, $id)
    {

        $items = Category::find($id);
        if ($request->name) {
            $items->name = $request->name;
        }
        if ($request->product_id) {
            $items->product_id = $request->product_id;
        }

        $items->save();
        return $items;
    }


    public function destroy($id)
    {
        $items = Category::find($id);
        $items->delete();
        return 'Deleted';
    }
}
