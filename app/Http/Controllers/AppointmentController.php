<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;

class AppointmentController extends Controller
{
    public function index()
    {
        $items = Appointment::all();
        return view('admin.appointments.index', compact('items'));
    }
    public function create()
    {
        return view('admin.appointments.create');
    } 
    public function store(Request $request)
    {
        $items = new Appointment();
        $items->user_id = $request->user_id;
        $items->date = $request->date;
        $items->center_id = $request->center_id;
        $items->save();
        return redirect()->route('appointment.index');
    }
    public function edit($id)
    {
        $items = Appointment::find($id);
        return view('admin.appointments.edit', compact('items'));
    }
    public function update(Request $request, $id)
    {
        $items = Appointment::find($id);
        if ($request->user_id) {
            $items->user_id = $request->user_id;
        }
        if ($request->date) {
            $items->date = $request->date;
        }
        if ($request->center_id) {
            $items->center_id = $request->center_id;
        }
        $items->save();
        return redirect()->route('appointment.index');
    }
    public function destroy($id)
    {
        $items = Appointment::find($id);
        $items->delete();
        return redirect()->route('appointment.index');
    }
}
