<!-- BEGIN: Vendor CSS-->
@if (session()->get('direction') === 'rtl' )
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/vendors-rtl.min.css')) }}" />
@else
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/vendors.min.css')) }}" />
  <link rel="stylesheet" href="{{ asset(mix('css/core.css')) }}" />

@endif

@yield('vendor-style')
<!-- END: Vendor CSS-->

<!-- BEGIN: Theme CSS-->
<link rel="stylesheet" href="{{ asset(mix('css/base/themes/dark-layout.css')) }}" />
<link rel="stylesheet" href="{{ asset(mix('css/base/themes/bordered-layout.css')) }}" />
<link rel="stylesheet" href="{{ asset(mix('css/base/themes/semi-dark-layout.css')) }}" />

@php $configData = Helper::applClasses(); @endphp

<!-- BEGIN: Page CSS-->
@if ($configData['mainLayoutType'] === 'horizontal')
  <link rel="stylesheet" href="{{ asset(mix('css/base/core/menu/menu-types/horizontal-menu.css')) }}" />
@else
  <link rel="stylesheet" href="{{ asset(mix('css/base/core/menu/menu-types/vertical-menu.css')) }}" />
@endif

{{-- Page Styles --}}
@yield('page-style')

<!-- laravel style -->
<link rel="stylesheet" href="{{ asset(mix('css/overrides.css')) }}" />

<!-- BEGIN: Custom CSS-->

@if (session()->get('direction') === 'rtl' )


  <link rel="stylesheet"  href="{{ asset('css-rtl/bootstrap.min.css') }}">
  <link rel="stylesheet"  href="{{ asset('css-rtl/bootstrap-extended.min.css') }}">
  <link rel="stylesheet"  href="{{ asset('css-rtl/colors.min.css') }}">
  <link rel="stylesheet"  href="{{ asset('css-rtl/components.min.css') }}">
  <link rel="stylesheet"  href="{{ asset('css-rtl/dark-layout.min.css') }}">
  <link rel="stylesheet"  href="{{ asset('css-rtl/bordered-layout.min.css') }}">
  <link rel="stylesheet"  href="{{ asset('css-rtl/semi-dark-layout.min.css') }}">

  <!-- BEGIN: Page CSS-->
  <link rel="stylesheet"  href="{{ asset('css-rtl/vertical-menu.min.css') }}">
  <link rel="stylesheet"  href="{{ asset('css-rtl/dashboard-ecommerce.min.css') }}">
  <link rel="stylesheet"  href="{{ asset('css-rtl/chart-apex.min.css') }}">


  <link rel="stylesheet" href="{{ asset(mix('css-rtl/custom-rtl.css')) }}" />
  <link rel="stylesheet" href="{{ asset(mix('css-rtl/style-rtl.css')) }}" />

@else
  {{-- user custom styles --}}
  <link rel="stylesheet" href="{{ asset(mix('css/style.css')) }}" />
@endif
