<!-- BEGIN: Vendor JS-->
<script src="{{ asset(mix('vendors/js/vendors.min.js')) }}"></script>
<!-- BEGIN Vendor JS-->
<!-- BEGIN: Page Vendor JS-->
<script src="{{asset(mix('vendors/js/ui/jquery.sticky.js'))}}"></script>
@yield('vendor-script')
<!-- END: Page Vendor JS-->
<!-- BEGIN: Theme JS-->
<script src="{{ asset(mix('js/core/app-menu.js')) }}"></script>
<script src="{{ asset(mix('js/core/app.js')) }}"></script>
{{-- <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script> --}}
{{-- <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase-auth.js"></script> --}}


<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.2/firebase.js"></script>

<script>
// Import the functions you need from the SDKs you need
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyDgHaKsQqT2jA1mINZMGihobLs9G2nWTx8",
  authDomain: "brokers-cc13e.firebaseapp.com",
  projectId: "brokers-cc13e",
  storageBucket: "brokers-cc13e.appspot.com",
  messagingSenderId: "410625571207",
  appId: "1:410625571207:web:7f89c2aeb522193418bb22"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

function startFCM() {
    messaging
        .requestPermission()
        .then(function () {
            console.log( messaging.getToken());
            return messaging.getToken()
        })
        .then(function (response) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                    url: '{{ route("store.token") }}',
                    type: 'POST',
                    data: {
                        token: response
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        console.log('done');
                    },
                    error: function (error) {
                    },
                });
            console.log(response);

        }).catch(function (error) {
        });
}

messaging.onMessage(function (payload) {
    console.log("a");
    console.log(payload);
    const title = payload.notification.title;
    const options = {
        body: payload.notification.body,
        icon: payload.notification.icon,
    };
    new Notification(title, options);
    const messaging = getMessaging(firebaseApp);

    console.log(payload);

});
    $.ajax({
        url: '{{ route("has.token") }}',
        type: 'get',
        success: function (response) {
            if(response==1);
                startFCM();
        },
        error: function (error) {
        },
    });


</script>
<!-- custome scripts file for user -->
<script src="{{ asset(mix('js/core/scripts.js')) }}"></script>

@if($configData['blankPage'] === false)
<script src="{{ asset(mix('js/scripts/customizer.js')) }}"></script>
@endif
<!-- END: Theme JS-->
<!-- BEGIN: Page JS-->
@yield('page-script')
<!-- END: Page JS-->
