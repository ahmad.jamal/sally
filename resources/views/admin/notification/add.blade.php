@extends('layouts/contentLayoutMaster')

@section('title', 'Add Notification')

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">

@endsection

@section('content')
<div class="content-header row">
    <div class="content-header-left col-md-6 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="">{{__('Home')}}</a></li>
                    <li class="breadcrumb-item"><a href=""> {{__('notification')}}</a></li>
                    <li class="breadcrumb-item active"> {{__('send notification')}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- Basic form layout section start -->
    <section id="basic-form-layouts">
        <div class="row match-height">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title" id="basic-layout-form"> {{__('Send notification')}}</h4>
                        <a class="heading-elements-toggle"><i
                                class="icon-ellipsis-v font-medium-3"></i></a>
                    </div>

                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form" action="{{route('admin.notification.send')}}" method="POST"
                                  enctype="multipart/form-data">
                                @csrf

                                <div class="form-body">

                                    <h4 class="form-section"><i
                                            class="ft-home"></i> {{__('notification details')}} </h4>


                                    <div class="form-group row">
                                        <label class="col-md-3 label-control"
                                               for="unit">{{__('notification title')}}</label>
                                        <div class="col-md-9">
                                            <input id="unit"
                                                   name="title"
                                                   class="form-control border-primary"
                                                   placeholder="{{__('notification title')}}"
                                                   value="{{ old('unit')}}"  required>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-5">
                                        <label class="col-md-3 label-control"
                                               for="unit">{{__('notification body')}}</label>
                                        <div class="col-md-9">
                                            <input id="unit"
                                                   name="body"
                                                   class="form-control border-primary"
                                                   placeholder="{{__('notification body')}}"
                                                   value="{{ old('unit')}}"  required>
                                        </div>
                                    </div>
                                    {{-- <table id="users" class="compact table mt-2">
                                        <thead>
                                          <tr>
                                            <th>{{ __('ID') }}</th>
                                            <th>{{ __('Name') }}</th>
                                            <th>{{ __('Email') }}</th>
                                            <th>{{ __('Date') }}</th>
                                            <th>{{ __('Role') }}</th>
                                            <th>{{ __('Action') }}</th>
                                          </tr>
                                        </thead>
                                          <body>
                                              @foreach ($users as $item)
                                              <tr>
                                                  <td>{{$item->id}}</td>
                                                  <td>{{$item->name}}</td>
                                                  <td>{{$item->email}}</td>
                                                  <td>{{$item->created_at}}</td>
                                                  @if (isset($item->getRoleNames()[0]))
                                                  <td>{{$item->getRoleNames()[0]}}</td>
                                                  @else
                                                  <td></td>
                                                  @endif

                                                  <td>
                                                      <div class="row col-12">
                                                          <div class="col-6">
                                                              <a href="{{route('user.edit',$item-> id)}}"
                                                                  class="btn btn-success">
                                                                   <i class="icon-pencil"></i> {{__('Edit')}}
                                                              </a>
                                                          </div>
                                                          <div class="col-6">
                                                              <form method="post"
                                                              action="{{route('user.delete',$item-> id)}}">
                                                              @csrf
                                                                  @method('DELETE')
                                                                  <button class="btn btn-danger"  onclick="return confirmBlock()"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                                              </form>
                                                          </div>
                                                      </div>
                                                  </td>
                                              </tr>
                                              @endforeach
                                          </body>
                                    </table> --}}

                                      <div class="col-md-6 mb-1">
                                        <label class="form-label" for="select2-multiple">Select Users</label>
                                        <select class="select2 form-select" name="users[]" id="select2-multiple" multiple>
                                            <option value="0" >All Users</option>
                                            @foreach ($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                      </div>
                                <div class="form-actions">

                                    <button type="submit" class="btn btn-primary block-page">
                                        <i class="icon-check"></i> {{__('Submit')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>

  <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>


@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
   $( document ).ready(function() {
 // console.log( $("#uploadImage").attr);
 $("#uploadImage5").change(function () {

if (this.files && this.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#uploadPreview5').attr('src', e.target.result);
    }
    reader.readAsDataURL(this.files[0]);
}
});
});
</script>
