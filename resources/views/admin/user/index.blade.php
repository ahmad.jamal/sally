@extends('layouts.contentLayoutMaster')


@section('title', __('Users'))






@section('content')
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary" role="alert">
      <div class="alert-body">
        <!-- Responsive Datatable -->

    <section id="responsive-datatable">
        <div class="row">
        <div class="col-12">
            <div class="card">
            <div class="card-header border-bottom">
                <h4 class="card-title">{{ __('Users') }}</h4>
                <a href="{{route('user.create')}}"><button type="button" class="btn btn-primary">{{ __('Add User') }}</button></a>
            </div>
            <div class="card-datatable">
                <table class="category compact table" id="defulte">
                <thead>
                    <tr>
                    <th></th>
                    <th>{{ __('Name') }}</th>
                    <th>{{ __('National Number') }}</th>
                    <th>{{ __('Date') }}</th>
                    <th>{{ __('Days Left') }}</th>
                    <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
                    <body>
                        @foreach ($items as $item)
                        <tr>
                            <td></td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->national_number}}</td>
                            <td>{{$item->created_at}}</td>
                            <td>{{$item->days}}</td>
                            <!-- @if (isset($item->getRoleNames()[1]))
                            <td>{{$item->getRoleNames()[1]}}</td>
                            @elseif (isset($item->getRoleNames()[0]))
                            <td>{{$item->getRoleNames()[0]}}</td>
                            @else
                            <td></td>
                            @endif -->

                            <td>
                                <div class="row col-12">

                                    <!-- <div class="col-4">
                                        <a href="{{route('user.edit',$item-> id)}}"
                                            class="btn btn-success">
                                            <i class="icon-pencil"></i> {{__('Edit')}}
                                        </a>
                                    </div> -->
                                    <div class="col-4">
                                        <a href="{{route('user.profile',$item-> id)}}"
                                            class="btn btn-success">
                                            <i class="icon-pencil"></i> {{__('Profile')}}
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <a href="{{route('shangeStatus', $item-> id)}}"
                                            class="btn btn-success">
                                            @if($item->pand == 1)
                                            <i class="icon-pencil"></i> {{__('UnBlock User')}}
                                            @else
                                            <i class="icon-pencil"></i> {{__('Block User')}}
                                            @endif

                                        </a>
                                    </div>
                                    <div class="col-4">
                                        <form method="post"
                                        action="{{route('user.delete',$item-> id)}}">
                                        @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger"  onclick="return confirmBlock()"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </body>
                </table>
            </div>
            </div>
        </div>
        </div>
    </section>
  <!--/ Responsive Datatable -->
      </div>
    </div>
  </div>
</div>
@endsection








@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection



@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>
@endsection
