@extends('layouts/contentLayoutMaster')

@section('title', __('Add Hospital'))

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary" role="alert">
      <div class="alert-body">
        <!-- Vertical Wizard -->
        <div class="card-body">
            <form  action="{{ route('hospital.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row col-12">
                    <div class="mb-1 col-6">
                        <label class="form-label" >{{ __('Name') }}</label>
                        <input
                          type="text"
                          class="form-control"
                          placeholder="Name"
                          name="name"
                          required
                        />
                      </div>
                      <div class="mb-1 col-6">
                        <label class="form-label" >{{ __('Email') }}</label>
                        <input
                          type="email"
                          class="form-control"
                          placeholder="Email"
                          name="email"
                          required
                        />
                      </div>
                </div>

                <div class="row col-12">
                    <div class="mb-1 col-4">
                        <label class="form-label" >{{ __('Password') }}</label>
                        <input
                          type="passwordstudent"
                          class="form-control"
                          placeholder="Password"
                          name="password"
                          required
                        />
                      </div>
                      
                      <div class="mb-1 col-4">
                        <label class="form-label" >{{ __('Phone') }}</label>
                        <input
                          type="phone"
                          class="form-control"
                          placeholder="Phone"
                          name="phone"
                          required
                        />
                      </div>

                      <div class="mb-1 col-4">
                        <label class="form-label" >{{ __('Location') }}</label>
                        <input
                          type="text"
                          class="form-control"
                          placeholder="Location"
                          name="address"
                          required
                        />
                      </div>

                <div class="row col-12">
                    
                  
                </div>

                <div id="div1" class="mb-2">
                    <div class="row col-12" >
                        <label style="    margin-top: 17px;" class="form-label" for="vertical-username">{{ __('Image') }}</label>
                        <div class="card-body">
                            <input class="form-control" id="uploadImage0" type="file" name="image" />
                            <img style="width: 129px; margin-top: 2%;" id="uploadPreview0" src="/" alt="your image" />
                        </div>
                    </div>
                </div>
                <div class="col-3 mb-2">
                    <button type="button" onclick="addImage()" class="btn btn-outline-secondary btn-prev">
                        {{ __('Add Image') }}
                    </button>
                </div>


              </div>
              <div class="row col-12">
              </div>
              <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
            </form>
          </div>
  <!-- /Vertical Wizard -->
      </div>
    </div>
  </div>
</div>
@endsection
@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    console.log("a");
    $('document').ready(function () {
        // console.log( $("#uploadImage").attr);
        $("#uploadImage").change(function () {

            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadPreview1').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        // $("#uploadImage2").change(function () {

        // if (this.files && this.files[0]) {
        //     var reader = new FileReader();
        //     reader.onload = function (e) {
        //         $('#uploadPreview2').attr('src', e.target.result);
        //     }
        //     reader.readAsDataURL(this.files[0]);
        // }
        // });
    });
</script>

