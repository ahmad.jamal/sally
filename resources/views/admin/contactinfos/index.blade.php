@extends('layouts/contentLayoutMaster')

@section('title', 'Contact Info')

@section('content')
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary" role="alert">
      <div class="alert-body">
        <!-- Responsive Datatable -->
        <section id="responsive-datatable">
            <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="card-header border-bottom">
                    <h4 class="card-title">Contact Info</h4>
                </div>
                <div class="card-datatable">
                    <table class="dt-responsive table">
                    <thead>
                        <tr>
                        <th></th>
                        <th>instagram</th>
                        <th>facebook</th>
                        <th>twitter</th>
                        <th>linkedin</th>
                        <th>Date</th>
                        <th>Salary</th>
                        <th>Age</th>
                        <th>Experience</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Post</th>
                        <th>City</th>
                        <th>Date</th>
                        <th>Salary</th>
                        <th>Age</th>
                        <th>Experience</th>
                        <th>Status</th>
                        </tr>
                    </tfoot>
                    </table>
                </div>
                </div>
            </div>
            </div>
        </section>
        <!--/ Responsive Datatable -->
      </div>
    </div>
  </div>
</div>
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>
@endsection
