@extends('layouts/contentLayoutMaster')

@section('title', __('Appointments'))

@section('content')
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary" role="alert">
      <div class="alert-body">
        <!-- Responsive Datatable -->

<section id="responsive-datatable">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header border-bottom">
            <h4 class="card-title">{{ __('Appointments Details') }}</h4>

          </div>
          <div class="card-datatable">
            <table class="category compact table" id="defulte">
              <thead>
                <tr>
                  <th></th>
                  <th>{{ __('User Name') }}</th>
                  <th>{{ __('Vaccine') }}</th>
                  {{-- <th>{{ __('Action') }}</th> --}}
                </tr>
              </thead>
                <body>
                    @foreach ($items as $item)
                    <tr>
                        <td></td>
                        @if(\App\Models\User::find($item->user_id))
                        <td>{{\App\Models\User::find($item->user_id)->name}}</td>
                        @else
                        <td>--</td>
                        @endif
                        @if(\App\Models\Vaccine::find($item->vaccine))
                        <td>{{\App\Models\Vaccine::find($item->vaccine)->name}}</td>
                        @else
                        <td>--</td>
                        @endif

                        {{-- <td>
                            <div class="row col-15">
                                <div class="col-2">
                                    <a href="{{route('medical_center.edit',$item-> id)}}"
                                        class="btn btn-success">
                                         <i class="icon-pencil"></i> {{__('Edit')}}
                                    </a>
                                </div>
                                <div class="col-2">
                                    <a href="{{route('medical_center.detail',$item-> id)}}"
                                        class="btn btn-success">
                                         <i class="icon-pencil"></i> {{__('Show Profile')}}
                                    </a>
                                </div>
                                <div class="col-3">
                                    <a href="{{route('medical_center_vaccine.assignVaccineToCenter',$item-> id)}}"
                                        class="btn btn-success">
                                         <i class="icon-pencil"></i> {{__('Assign Vaccine')}}
                                    </a>
                                </div>
                                <div class="col-3">
                                    <a href="{{route('medical_center_vaccine.showVaccine',$item-> id)}}"
                                        class="btn btn-success">
                                         <i class="icon-pencil"></i> {{__('Show Vaccines')}}
                                    </a>
                                </div>
                                <div class="col-2">
                                    <form method="post"
                                    action="{{route('medical_center.delete',$item-> id)}}">
                                    @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger"  onclick="return confirmBlock()"><i class="icon-trash"></i>  {{__('Delete')}}</button>
                                    </form>
                                </div>
                            </div>
                        </td> --}}
                    </tr>
                    @endforeach
                </body>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--/ Responsive Datatable -->
      </div>
    </div>
  </div>
</div>
@endsection
@section('vendor-script')
{{-- vendor files --}}
  <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/tables/table-datatables-advanced.js')) }}"></script>
@endsection
