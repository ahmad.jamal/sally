@extends('layouts/contentLayoutMaster')

@section('title',  __('Edit Medical Center') )

@section('vendor-style')
  <!-- vendor css files -->
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/wizard/bs-stepper.min.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('page-style')
  <!-- Page css files -->
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-wizard.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-file-uploader.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/pickers/form-flat-pickr.css')) }}">
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="alert alert-primary" role="alert">
      <div class="alert-body">
        <!-- Vertical Wizard -->
        <div class="card-body">
            <form  action="{{ route('medical_center.update', $items->id) }}" method="post" enctype="multipart/form-data">
                @csrf
              <div class="mb-1">

                <div class="row col-12">
                    <div class="mb-1 col-6">
                        <label class="form-label" >{{ __('Name') }}</label>
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Name"
                            name="name"
                            value="{{$items->name}}"
                            required
                        />
                      </div>
                      <div class="mb-1 col-6">
                        <label class="form-label" >{{ __('Address') }}</label>
                        <input
                            type="text"
                            class="form-control"
                            placeholder="Address"
                            name="address"
                            value="{{$items->address}}"
                            required
                        />
                      </div>
                      
               
                      
                </div>
                <div class="row col-12">
                <div class="mb-1 col-6">
                        <label class="form-label" >{{ __('Phone') }}</label>
                        <input
                            type="phone"
                            class="form-control"
                            placeholder="Phone"
                            name="phone"
                            value="{{$items->phone}}"
                            required
                        />
                      </div>
                      <div class="mb-1 col-6">
                        <label class="form-label" >{{ __('Time Open') }}</label>
                        <input
                            type="time"
                            class="form-control"
                            placeholder="Time Open"
                            name="time_open"
                            value="{{$items->time_open}}"
                            required
                        />
                      </div>

                </div>
              </div>
              </div>
              <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
            </form>
          </div>
  <!-- /Vertical Wizard -->
      </div>
    </div>
  </div>
</div>
@endsection
@section('vendor-script')
  <!-- vendor files -->
  <script src="{{ asset(mix('vendors/js/forms/wizard/bs-stepper.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/file-uploaders/dropzone.min.js')) }}"></script>
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection
@section('page-script')
  <!-- Page js files -->
  <script src="{{ asset(mix('js/scripts/forms/form-wizard.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-file-uploader.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/forms/form-validation.js')) }}"></script>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    console.log("a");
    $('document').ready(function () {
        // console.log( $("#uploadImage").attr);
        $("#uploadImage6").change(function () {
            console.log("a");
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#uploadPreview6').attr('src', e.target.result);
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
        // $("#uploadImage2").change(function () {
        //     console.log("a");
        //     if (this.files && this.files[0]) {
        //         var reader = new FileReader();
        //         reader.onload = function (e) {
        //             $('#uploadPreview2').attr('src', e.target.result);
        //         }
        //         reader.readAsDataURL(this.files[0]);
        //     }
        // });
    });
</script>
