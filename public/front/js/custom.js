$(window).on('scroll', function(event) {
    var scrollValue = $(window).scrollTop();
    if (scrollValue > 250  ) {
        $('.main-menu').addClass('cus-fixed-top');
    }  else {
        $('.main-menu').removeClass('cus-fixed-top');
    };
});

/* top search */
$("#top-search-filters li>span").click(function(){
    $("#top-search-filters li>span").removeClass('active');
    $(this).addClass('active');

});
$("#top-search-filters1 li>span").click(function(){
    $("#top-search-filters1 li>span").removeClass('active');
    $(this).addClass('active');

});
$("#top-search-filters li>span").click(function(){
    var filter_id = $(this).prop('id');
    $("#top-search-products > div").hide();
    $("#top-search-products > div").each(function(){
        if($(this).hasClass(filter_id) ) {

            $(this).show();
        }
    });
});
$("#top-search-filters li>span#all").click(function(){
    $("#top-search-products > div").show()
});
$("#top-search-filters1 li>span#all").click(function(){
    $("#top-search-products1 > div").show()
});
$("#top-search-filters1 li>span").click(function(){
    var filter_id = $(this).prop('id');
    $("#top-search-products1 > div").hide();
    $("#top-search-products1 > div").each(function(){
        if($(this).hasClass(filter_id) ) {

            $(this).show();
        }
    });
});
$("#top-search-filters li>span#all").click(function(){
    $("#top-search-products1 > div").show()
});

$("#top-search-filters1 li>span#all").click(function(){
    $("#top-search-products1 > div").show()
});
/*******************************/

$('#main-slider').owlCarousel({
    loop:true,
    margin:0,
    items: 1,
    rtl: true,
    autoplay: true,
    nav:false,
    dots: true
})


$('#top-selling-slider').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider0').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider1').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider2').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider3').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider4').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider5').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider6').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider7').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider8').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider9').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#top-selling-slider10').owlCarousel({
    loop:true,
    margin:15,
    items: 4,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: false,
    responsive:{
        0:{
            items:1,
        },
        768:{
            items:2,
        },
        1000:{
            items:4,
        }
    }
})
$('#product-images').owlCarousel({
    loop:true,
    margin:0,
    items: 1,
    rtl: true,
    autoplay: true,
    nav:true,
    dots: true
});

/*******plus minus ********/

$('.btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {

            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the maximum value was reached');
        $(this).val($(this).data('oldValue'));
    }


});
$(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    /*******************************/
    $('.showpassword').click(function(){
        $(this).toggleClass('show');
 
    });
    $('.showpassword1').click(function(){
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
            } else {
            x.type = "password";
        }
    });
    
    $('.showpassword0').click(function(){
        var y = document.getElementById("password0");

        if (y.type === "password") {
            y.type = "text";
            } else {
            y.type = "password";
        }
    });
    
    $('.showpassword2').click(function(){
        var z = document.getElementById("password2");

        if (z.type === "password") {
            z.type = "text";
            } else {
            z.type = "password";
        }
    });


    $('select').select2({
        placeholder: "Select a State",
        minimumResultsForSearch: -1
      });
