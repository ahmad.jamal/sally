<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HospitalApiController;
use App\Http\Controllers\PharmacyApiController;
use App\Http\Controllers\MinistryApiController;
use App\Http\Controllers\BrandApiController;
use App\Http\Controllers\AppointmentApiController;
use App\Http\Controllers\ProductApiController;
use App\Http\Controllers\UserApiController;
use App\Http\Controllers\VaccineApiController;
use App\Http\Controllers\Medical_centerApiController;
use App\Http\Controllers\CategoryApiController;
use App\Http\Controllers\UserApiController1;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/user', [UserApiController1::class, 'index']);
Route::get('/user/show/{id}', [UserApiController1::class, 'show']);
Route::post('user/store', [UserApiController1::class, 'store']);
Route::post('user/login', [UserApiController1::class, 'login']);

Route::post('user/image/store', [UserApiController1::class, 'imageStore']);
Route::post('user/voice/store', [UserApiController1::class, 'voiceStore']);
Route::delete('user/delete/{id}', [UserApiController1::class, 'destroy']);
Route::post('user', [UserApiController1::class, 'update']);


Route::get('/hospital', [HospitalApiController::class, 'index']);
Route::get('/hospital/show/{id}', [HospitalApiController::class, 'show']);
Route::post('hospital/store', [HospitalApiController::class, 'store']);
Route::delete('hospital/delete/{id}', [HospitalApiController::class, 'destroy']);
Route::post('hospital/{id}', [HospitalApiController::class, 'update']);


Route::get('/pharmacy', [PharmacyApiController::class, 'index']);
Route::get('/pharmacy/show/{id}', [PharmacyApiController::class, 'show']);
Route::post('pharmacy/store', [PharmacyApiController::class, 'store']);
Route::delete('pharmacy/delete/{id}', [PharmacyApiController::class, 'destroy']);
Route::post('pharmacy/{id}', [PharmacyApiController::class, 'update']);

Route::get('/ministry', [MinistryApiController::class, 'index']);
Route::get('/ministry/show/{id}', [MinistryApiController::class, 'show']);
Route::post('ministry/store', [MinistryApiController::class, 'store']);
Route::delete('ministry/delete/{id}', [MinistryApiController::class, 'destroy']);
Route::post('ministry/{id}', [MinistryApiController::class, 'update']);

Route::get('/brand', [BrandApiController::class, 'index']);
Route::get('/brand/show/{id}', [BrandApiController::class, 'show']);
Route::post('brand/store', [BrandApiController::class, 'store']);
Route::delete('brand/delete/{id}', [BrandApiController::class, 'destroy']);
Route::post('brand/{id}', [BrandApiController::class, 'update']);

Route::get('/appointment', [AppointmentApiController::class, 'index']);
Route::get('/appointment/show/{id}', [AppointmentApiController::class, 'show']);
Route::post('appointment/store', [AppointmentApiController::class, 'store']);
Route::delete('appointment/delete/{id}', [AppointmentApiController::class, 'destroy']);
Route::post('appointment/{id}', [AppointmentApiController::class, 'update']);

Route::get('/product', [ProductApiController::class, 'index']);
Route::get('/product/show/{id}', [ProductApiController::class, 'show']);
Route::post('product/store', [ProductApiController::class, 'store']);
Route::delete('product/delete/{id}', [ProductApiController::class, 'destroy']);
Route::post('product/{id}', [ProductApiController::class, 'update']);


Route::get('/vaccine', [VaccineApiController::class, 'index']);
Route::get('/vaccine/show/{id}', [VaccineApiController::class, 'show']);
Route::post('vaccine/store', [VaccineApiController::class, 'store']);
Route::delete('vaccine/delete/{id}', [VaccineApiController::class, 'destroy']);
Route::post('vaccine/{id}', [VaccineApiController::class, 'update']);



Route::get('/medical_center', [Medical_centerApiController::class, 'index']);
Route::get('/medical_center/show/{id}', [Medical_centerApiController::class, 'show']);
Route::get('/medical_center/{id}', [Medical_centerApiController::class, 'showVaccinee']);
Route::post('medical_center/store', [Medical_centerApiController::class, 'store']);
Route::delete('medical_center/delete/{id}', [Medical_centerApiController::class, 'destroy']);
Route::post('medical_center/{id}', [Medical_centerApiController::class, 'update']);


Route::get('/category', [CategoryApiController::class, 'index']);
Route::get('/category/show/{id}', [CategoryApiController::class, 'show']);
Route::post('category/store', [CategoryApiController::class, 'store']);
Route::delete('category/delete/{id}', [CategoryApiController::class, 'destroy']);
Route::post('category/{id}', [CategoryApiController::class, 'update']);
