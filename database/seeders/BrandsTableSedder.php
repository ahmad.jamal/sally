<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Directory;
use Illuminate\Support\Facades\DB;

class BrandsTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('brands')->insert([
                'nameAr' => $faker->name(),
                'name' => $faker->name(),
                'description' => $faker->paragraph($nb = 8),
                'descriptionAr' => $faker->paragraph($nb = 8)
            ]);
        }
    }
}
