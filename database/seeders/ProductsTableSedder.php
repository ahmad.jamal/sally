<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Directory;
use Illuminate\Support\Facades\DB;

class ProductsTableSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 200) as $index) {
            DB::table('products')->insert([
                'nameAR' => $faker->name(),
                'nameEN' => $faker->name(),
                'slug' => $faker->unique()->slug,
                'price' => $faker->numberBetween($min = 500, $max = 8000),
                'image' => $faker->imageUrl($width = 400, $height = 400),
                'category_id' => $faker->numberBetween($min = 0, $max = 100),
                'brand_id' => $faker->numberBetween($min = 0, $max = 100),
                'descriptionAR' => $faker->paragraph($nb = 8),
                'descriptionEN' => $faker->paragraph($nb = 8)
            ]);
        }
    }
}
